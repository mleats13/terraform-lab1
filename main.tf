terraform {
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.27"
      }
  }
}

provider "aws" {
    profile = "default"
    region = "us-east-2"
}

resource "aws_instance" "test" {
    ami = "ami-09246ddb00c7c4fef"
    #ami = "ami-08962a4068733a2b6"
    instance_type = "t2.micro"

    tags = {
      #Name = "MySecondTerraformInstance"
      Name = var.instance_name
    }
}